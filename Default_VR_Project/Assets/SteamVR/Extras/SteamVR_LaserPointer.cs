﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
using UnityEngine;
using System.Collections;
using Valve.VR.InteractionSystem;
using System.Runtime.InteropServices.WindowsRuntime;

namespace Valve.VR.Extras
{
    public class SteamVR_LaserPointer : MonoBehaviour
    {
        public SteamVR_Behaviour_Pose pose;
        public WhichHand hand;
        //public SteamVR_Action_Boolean interactWithUI = SteamVR_Input.__actions_default_in_InteractUI;
        public SteamVR_Action_Boolean interactWithUI = SteamVR_Input.GetBooleanAction("InteractUI");

        public bool active = true;
        public Color color;
        public float thickness = 0.002f;
        public Color clickColor = Color.green;
        public GameObject holder;
        public GameObject pointer;
        bool isActive = false;
        public bool addRigidBody = false;
        public Transform reference;
        public event PointerEventHandler PointerIn;
        public event PointerEventHandler PointerOut;
        public event PointerEventHandler PointerClick;

        Transform previousContact = null;

        private void Start()
        {
            if (pose == null)
                pose = this.GetComponent<SteamVR_Behaviour_Pose>();
            if (pose == null)
                Debug.LogError("No SteamVR_Behaviour_Pose component found on this object", this);

            if (interactWithUI == null)
                Debug.LogError("No ui interaction action has been set on this component.", this);
            
            if (holder == null)
            {
                holder = new GameObject();
                holder.transform.parent = this.transform;
                holder.transform.localPosition = Vector3.zero;
                holder.transform.localRotation = Quaternion.identity;
            }

            if (pointer == null)
            {
                pointer = GameObject.CreatePrimitive(PrimitiveType.Cube);
                pointer.transform.parent = holder.transform;
                pointer.transform.localScale = new Vector3(thickness, thickness, 100f);
                pointer.transform.localPosition = new Vector3(0f, 0f, 50f);
                pointer.transform.localRotation = Quaternion.identity;
            }

            BoxCollider collider = pointer.GetComponent<BoxCollider>();
            if (addRigidBody)
            {
                if (collider)
                {
                    collider.isTrigger = true;
                }
                Rigidbody rigidBody = pointer.AddComponent<Rigidbody>();
                rigidBody.isKinematic = true;
            }
            else
            {
                if (collider)
                {
                    Object.Destroy(collider);
                }
            }
            Material newMaterial = new Material(Shader.Find("Unlit/Color"));
            newMaterial.SetColor("_Color", color);
            pointer.GetComponent<MeshRenderer>().material = newMaterial;
        }

        public virtual void OnPointerIn(PointerEventArgs e)
        {
            if (PointerIn != null)
                PointerIn(this, e);
        }

        public virtual void OnPointerClick(PointerEventArgs e)
        {
            if (PointerClick != null)
                PointerClick(this, e);
        }

        public virtual void OnPointerOut(PointerEventArgs e)
        {
            if (PointerOut != null)
                PointerOut(this, e);
        }


        private void Update()
        {
            if (!isActive)
            {
                isActive = true;
                this.transform.GetChild(0).gameObject.SetActive(true);
            }

            float dist = 20;

            int layerMask = 1 << 8;  // This would cast rays only against colliders in layer 8. 
            layerMask = ~layerMask;  // But instead we want to collide against everything except layer 8. The ~ operator does this, it inverts a bitmask.
            RaycastHit hit;


            Ray raycast = new Ray(holder.transform.position, holder.transform.forward);
            Debug.DrawRay(holder.transform.position, holder.transform.forward, Color.green);

            bool bHit = Physics.Raycast(raycast, out hit, 100f, layerMask);
            //print(hit.collider);
            SideSpecificeLaser(hand, hit);

          
        }
        void SideSpecificeLaser(WhichHand hand, RaycastHit hit)
        {
            float dist=10;
            bool squeeze = false;
            GameObject TargetCross = null;
            switch (hand)
            {
                case WhichHand.Left:
                    squeeze = ControllerHandler_Vive.B_Squeeze_Left;
                    //#TargetCross = ControllerHandler_Vive.TargetCross_L;

                    break;
                case WhichHand.Right:
                    squeeze = ControllerHandler_Vive.B_Squeeze_Right;
                    //TargetCross = ControllerHandler_Vive.TargetCross_R;
                    break;
            }
            if (TargetCross != null)
            {
                TargetCross.transform.rotation = pointer.transform.rotation;
            }

            Vector3 newScale = new Vector3(0, 0, dist);

            if (hit.collider != null)
            {
                print(hit.collider.gameObject);
                dist = hit.distance;

                if (TargetCross != null)
                {
                    TargetCross.transform.position = pointer.transform.position;
                }
                if (squeeze)
                {
                    newScale= new Vector3(thickness * 5f, thickness * 5f, dist);
                    pointer.GetComponent<MeshRenderer>().material.color = clickColor;
                }
                else
                {
                    newScale = new Vector3(0, 0, dist);
                    pointer.GetComponent<MeshRenderer>().material.color = color;
                }
            }
            else
            {
                pointer.GetComponent<MeshRenderer>().material.color = color;
                if (TargetCross != null)
                {
                    TargetCross.transform.position = pointer.transform.position;
                }
            }
            pointer.transform.localScale = newScale;

            pointer.transform.localPosition = new Vector3(0f, 0f, dist / 2f);
        }
    }



    public struct PointerEventArgs
    {
        public SteamVR_Input_Sources fromInputSource;
        public uint flags;
        public float distance;
        public Transform target;
    }

    public delegate void PointerEventHandler(object sender, PointerEventArgs e);
}