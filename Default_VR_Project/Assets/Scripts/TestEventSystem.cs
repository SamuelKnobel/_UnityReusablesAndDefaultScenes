﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Bhaptics.Tact.Unity;
using UnityEngine.XR;

public class TestEventSystem : MonoBehaviour
{

    void OnEnable()
    {
        EventManager.EventFloat += TestFunction3;
        EventManager.TriggerLeftEvent += TestFunction4;
        EventManager.TriggerRightEvent += TestFunction5;
        EventManager.ColliderInteractionEvent += TestFunction3;

    }

    void OnDisable()
    {
        EventManager.EventFloat -= TestFunction3;
        EventManager.TriggerLeftEvent -= TestFunction4;
        EventManager.TriggerRightEvent -= TestFunction5;
        EventManager.ColliderInteractionEvent -= TestFunction3;

    }


    private void Awake()
    {
        DefineHMD();
        // TODO: make  things dependent from Type of HMD -->Use ENUM
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
       
    }
    void DefineHMD()
    {
        string hardware;

        //string model = UnityEngine.VR.VRDevice.model != null ? UnityEngine.VR.VRDevice.model : "";
        string model = UnityEngine.XR.XRDevice.model != null ? UnityEngine.XR.XRDevice.model : "";
        if (model.IndexOf("Vive") >= 0)
        {
            hardware = "htc_vive";
        }
        else
        {
            hardware = "Others";
        }

        //print(UnityEngine.XR.XRDevice.model);

    }

    void TestFunction3(float unused)
    {
        print("Event3 Called, Trigger pressed");
        print(unused);
    }    
    void TestFunction4()
    {
        print("Event4 Called, Trigger pressed");
    }      
    void TestFunction5()
    {
        print("Event5 Called, Trigger pressed");
    }    
    void TestFunction3(GameObject unused)
    {
        //if (unused.tag == "")
        //{

        //}
        print("Event3 Called");
        print(unused.name);
    }




}
