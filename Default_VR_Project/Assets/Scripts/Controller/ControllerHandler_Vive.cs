﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;


public class ControllerHandler_Vive : MonoBehaviour
{
    public static ControllerHandler_Vive instance;
    [SerializeField]
    GameObject LeftController, RightController;
    [SerializeField]
    public GameObject TargetCross_L, TargetCross_R;
    [SerializeField]
    LineRenderer LaserLeft, LaserRight;

    bool b_isConnected_left;
    bool B_isConected_Left
    {
        get { return b_isConnected_left; }
        set
        {
            if (value == b_isConnected_left)
                return;
            b_isConnected_left = value;
            if (!b_isConnected_left)
            {
                ControllerNameLeft = ControllorNotFound;
            }
            else if (b_isConnected_left)
            { 
            }
        }
    }

    bool b_isConnected_right;
    bool B_isConected_Right
    {
        get { return b_isConnected_right; }
        set
        {
            if (value == b_isConnected_right)
                return;
            b_isConnected_right = value;
            if (!b_isConnected_right)
            {
                ControllerNameRight = ControllorNotFound;
            }
            else if (b_isConnected_right)
            {

            }
        }
    }

    string ControllerNameLeft = ControllorNotFound;
    string ControllerNameRight= ControllorNotFound;
    const string ControllorNotFound = "Not Found";

    [SerializeField]
    public bool b_squeeze_left, b_squeeze_right;

    [SerializeField]
    bool b_touch_left, b_touch_right;

    public static bool B_Squeeze_Left
    {
        get { return ControllerHandler_Vive.instance.b_squeeze_left; }
        set
        {
            if (value == instance.b_squeeze_left)
                return;

            instance.b_squeeze_left = value;
            if (instance.b_squeeze_left)
            {
                EventManager.CallTriggerLeftEvent();
            }
        }
    }
    public static bool B_Squeeze_Right
    {
        get { return instance.b_squeeze_right; }
        set
        {
            if (value == instance.b_squeeze_right)
                return;

            instance.b_squeeze_right = value;
            if (instance.b_squeeze_right)
            {
                EventManager.CallTriggerRightEvent();
            }
        }
    }

    bool B_Touch_Left
    {
        get { return b_touch_left; }
        set
        {
            if (value == b_touch_left)
                return;

            b_touch_left = value;
            if (b_touch_left)
            {
                EventManager.CallTouchLeftEvent();
            }
        }
    }
    bool B_Touch_Right
    {
        get { return b_touch_right; }
        set
        {
            if (value == b_touch_right)
                return;

            b_touch_right = value;
            if (b_touch_right)
            {
                EventManager.CallTouchRightEvent();
            }
        }
    }

    Vector3 startposition_L, startposition_R;
    Vector3 direction_L, direction_R;
    Vector3 end_L, end_R;

    [SerializeField]
    float Laserdistance;
    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        //points = new Vector3[2];
        //points[0] = Vector3.zero;

        //points[1] = transform.position + new Vector3(0, 0, 20);
        //rend.SetPositions(points);
        //rend.enabled = true;
    }
    /// <summary>
    /// Function to get left and right controller and their LineRenderer Component
    ///  TODO: Check if there is a Line Renderer
    /// </summary>
  

    // Update is called once per frame
    void Update()
    {
        HandelController();
        b_squeeze_left = getSqueeze_left();
        b_squeeze_right = getSqueeze_right();
        B_Touch_Left = getTouchPadLeft();
        B_Touch_Right = getTouchPadRight();

        createLaser(ConfigurationUtils.UseLaser);

       TargetCross_L.SetActive(ConfigurationUtils.UseTargetCross && B_isConected_Left);       
       TargetCross_R.SetActive(ConfigurationUtils.UseTargetCross && B_isConected_Right);       
    }
    

    /// <summary>
    /// Combines mutliple functions:
    /// - Findes the Controller GameObject
    /// - FIndes the Name of the Controllers
    /// - Checks if the Controller are connected
    /// </summary>
    void HandelController()
    {
        FindController();
        FindTargetCross();

        string[] JoyNames = Input.GetJoystickNames();
        if (ControllerNameLeft.Equals(ControllorNotFound))
        {
            ControllerNameLeft = ControllorNotFound;
            foreach (string JoystickNames in JoyNames)
            {
                if (JoystickNames.Contains("Controller") & JoystickNames.Contains("Left"))
                {
                    ControllerNameLeft = JoystickNames;
                    break;
                }
            }
        }
        if (ControllerNameRight.Equals(ControllorNotFound))
        {
            ControllerNameRight = ControllorNotFound;

            foreach (string JoystickNames in JoyNames)
            {
                if (JoystickNames.Contains("Controller") & JoystickNames.Contains("Right"))
                {
                    ControllerNameRight = JoystickNames;
                    break;
                }
            }
        }
        if (System.Array.IndexOf(JoyNames, ControllerNameLeft) != -1)
            B_isConected_Left = true;
        else
            B_isConected_Left = false;

        if (System.Array.IndexOf(JoyNames, ControllerNameRight) != -1)
            B_isConected_Right = true;
        else
            B_isConected_Right = false;
    }
    void FindController()
    {
        if (LeftController == null || RightController == null)
        {
            Debug.Log("Looking for Controller");
            GameObject[] Controllers = GameObject.FindGameObjectsWithTag("Controller");
            if (Controllers.Length == 0)
            {
                Debug.LogError("No Controllers Taged");
            }
            else if ((Controllers.Length > 2))
            {
                Debug.LogError("To many Controllers Taged");
                foreach (var item in Controllers)
                {
                    Debug.Log("Contrroller:" + item.name);
                }
            }
            else
            {
                for (int i = 0; i < Controllers.Length; i++)
                {
                    if (Controllers[i].name.Contains("left"))
                    {
                        LeftController = Controllers[i];
                        LaserLeft = LeftController.GetComponentInChildren<LineRenderer>();
                        LaserLeft.enabled = true;

                    }
                    else if (Controllers[i].name.Contains("right"))
                    {
                        RightController = Controllers[i];
                        LaserRight = RightController.GetComponentInChildren<LineRenderer>();
                        LaserRight.enabled = true;
                    }
                    else
                    {
                        Debug.Log("No Left or right Controller found, please check Name of Controller");
                    }
                }
            }
        }

    }
    void FindTargetCross()
    {

        if (TargetCross_L == null || TargetCross_R == null)
        {
            Debug.Log("Looking for TargetCross");
            GameObject[] AllTargetCross = GameObject.FindGameObjectsWithTag("TargetCross");
            if (AllTargetCross.Length == 0)
            {
                Debug.LogError("No TargetCross Taged");
            }
            else if ((AllTargetCross.Length > 2))
            {
                Debug.LogError("To many TargetCross Taged");
                foreach (var item in AllTargetCross)
                {
                    Debug.Log("Contrroller:" + item.name);
                }
            }
            else
            {
                for (int i = 0; i < AllTargetCross.Length; i++)
                {
                    if (AllTargetCross[i].name.Contains("_R"))
                    {
                        TargetCross_R = AllTargetCross[i];
                    }
                    else if (AllTargetCross[i].name.Contains("_L"))
                    {
                        TargetCross_L = AllTargetCross[i];
                    }
                    else
                    {
                        Debug.Log("No Left or right TargetCross found, please check Name of TargetCross");
                    }
                }
            }
        }

    }

    //Vector3[] points;

    void SideSpecificLaser(GameObject controller, GameObject targetCross, LineRenderer laser, bool squeeze)
    {
        //print(controller.name +":" + controller.transform.position);
        Ray ray = new Ray(controller.transform.position, controller.transform.forward);
        Debug.DrawRay(controller.transform.position, controller.transform.forward, Color.green);
        targetCross.transform.rotation = controller.transform.rotation;

        int layerMask = 1 << 8;  // This would cast rays only against colliders in layer 8. 
        layerMask = ~layerMask;  // But instead we want to collide against everything except layer 8. The ~ operator does this, it inverts a bitmask.

        RaycastHit hit;

        laser.SetPosition(0, controller.transform.position);
        targetCross.transform.localScale = Vector3.one * 0.2f;
        if (Physics.Raycast(ray, out hit, 100f, layerMask))
        {
            targetCross.transform.position = controller.transform.position + controller.transform.forward * 0.95f*Vector3.Distance(controller.transform.position, hit.point);
            targetCross.transform.localScale *= Mathf.Sqrt(hit.distance);
            targetCross.transform.localScale = new Vector3(targetCross.transform.localScale.x, targetCross.transform.localScale.y, 0.001f);
        }
        else
        {
            targetCross.transform.position = controller.transform.position + controller.transform.forward * 1;
        }

        if (squeeze || Input.GetKey(KeyCode.L))
        {
            if (hit.collider != null)
            {
                EventManager.CallColliderInteractionEvent(hit.collider.gameObject);
                EventManager.CallStringEvent(hit.collider.gameObject.name);
            }

            laser.SetPosition(1, targetCross.transform.position);
        }
        else
        {
            laser.SetPosition(1, controller.transform.position);
        }
    }

    /// <summary>
    /// Creates the Laser, dependent if the useLaser is true or false
    /// ToDo: Define Distance where the laser is
    /// </summary>
    /// <param name="useLaser"></param>
    public void createLaser(bool useLaser)
    {
        if (useLaser)
        {
            SideSpecificLaser(LaserLeft.gameObject, TargetCross_L, LaserLeft, B_Squeeze_Left);
            SideSpecificLaser(LaserRight.gameObject, TargetCross_R, LaserRight, B_Squeeze_Right);
        }
    }




    bool getSqueeze_left()
    {
        if (SteamVR_Actions._default.Squeeze.GetAxis(SteamVR_Input_Sources.LeftHand) > 0.02)
            return true;
        else
            return false;
    }  
    bool getSqueeze_right()
    {
        if (SteamVR_Actions._default.Squeeze.GetAxis(SteamVR_Input_Sources.RightHand) > 0.02)
                return true;
        else
            return false;

    }

    bool getTouchPadRight()
    {
        if (SteamVR_Actions._default.Teleport.GetState(SteamVR_Input_Sources.RightHand))
            return true;
        else
            return false;

    }
    bool getTouchPadLeft()
    {
        if (SteamVR_Actions._default.Teleport.GetState(SteamVR_Input_Sources.LeftHand))
            return true;
        else
            return false;

    }
}

