﻿using Bhaptics.Tact;
using Bhaptics.Tact.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestTactal : MonoBehaviour
{

    // Haptic feedback
    public GameObject TactalPrefab;
    public HapticSource HeadTactSource;
    public Tactal HeadBand;
    public BhapticsManager hapticPlayer;
    public bool TactalConnected;
   
    // Start is called before the first frame update
    void Start()
    {
        InitTactal();
    }

    // Update is called once per frame
    void Update()
    {
        TactalConnected = TactalIsConnected();
        if (Input.GetKey(KeyCode.Space))
        {
            HeadTactSource.clip.Play();
        }

        // TODO: Play tactal  if SpawneNew Target Event is happening-->event has to be sent 
        // with the angel where the object aperars. Additionally a Timer starts to cut the vibration    

        if (Input.GetKey(KeyCode.Alpha1))
        {
            //HeadTactSource.Play();
            HeadBand.Play(-10);
        }      
        if (Input.GetKey(KeyCode.Alpha2))
        {
            //HeadTactSource.Play();
            HeadBand.Play(10);
        }
        if (Input.GetKey(KeyCode.Alpha3))
        {
            //HeadTactSource.Play();
            HeadBand.Play(50);
        }
        if (Input.GetKey(KeyCode.Alpha4))
        {
            //HeadTactSource.Play();
            HeadBand.Play(-50);
        }

    }

    public void InitTactal()
    {
        //hapticPlayer = BhapticsManager.GetHaptic();

        // Instantiate HapticSource object from HapticMotorPrefab
        GameObject TactalObj = Instantiate(TactalPrefab, GameObject.Find("Testing").transform);
        HeadTactSource = TactalObj.GetComponent<HapticSource>();

        //HeadTactSource.
        //// Create the head band object
        HeadBand = new Tactal(ref HeadTactSource);

        //// Set default tactal parameters
        HeadBand.maxIntensityPerc = 50;
        HeadBand.mode = VibrationMode.SACCADIC;
    }

    public bool TactalIsConnected()
    {
        return false;
        //return BhapticsManager.HapticPlayer.IsActive(PositionType.Head);
    }

}
