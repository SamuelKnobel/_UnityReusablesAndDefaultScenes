﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VR_UI : MonoBehaviour
{
    public GameObject TestObject;


    void OnEnable()
    {

        EventManager.EventStringt += ButtonFunction;

    }

    void OnDisable()
    {
        EventManager.EventStringt -= ButtonFunction;


    }



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ButtonFunction(string ButtonName)
    {
        switch(ButtonName)
        {
            case "btn_red":
                TestObject.GetComponent<MeshRenderer>().material.color = Color.red;
                 break;       
            case "btn_blue":
                TestObject.GetComponent<MeshRenderer>().material.color = Color.blue;

                break;   
            case "btn_green":
                TestObject.GetComponent<MeshRenderer>().material.color = Color.green;

                break;    
            default:
                Debug.Log("Not Defined");
                break;
        }
    }

}
