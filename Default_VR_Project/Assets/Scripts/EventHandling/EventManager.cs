﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public static class  EventManager 
{

    #region Testing
    // For Testing
    //public delegate void GOEventAction(GameObject GO);
    //public static event GOEventAction GOEvent;


    //public static void CallGOEvent(GameObject GO)
    //{
    //    GOEvent?.Invoke(GO);
    //}
    #endregion


    #region Events without Input
    public delegate void EventAction();
    public static event EventAction TriggerLeftEvent;
    public static event EventAction TriggerRightEvent;

    public static event EventAction TouchLeftEvent; // NO listener yet
    public static event EventAction TouchRightEvent;// NO listener yet

    public static void CallTriggerLeftEvent()
    {
        TriggerLeftEvent?.Invoke();
    }    
    public static void CallTriggerRightEvent()
    {
        TriggerRightEvent?.Invoke();
    }
    public static void CallTouchLeftEvent()
    {
        if (TouchLeftEvent != null)
        {
            TouchLeftEvent.Invoke();
        }
        else
        {
            Debug.LogWarning("No Listener for RightTouchEvent");
        }
    }
    public static void CallTouchRightEvent()
    {
        if (TouchRightEvent != null)
        {
            TouchRightEvent.Invoke();
        }
        else
        {
            Debug.LogWarning("No Listener for RightTouchEvent");
        }
    }

    #endregion


    #region Events with Float Input
    public delegate void FloatEventAction(float inp);
    public static event FloatEventAction EventFloat;


    public static void CallFloatEvent(float Float)
    {
        EventFloat?.Invoke(Float);
    }

    #endregion

    #region Events with String Input
    public delegate void StringEventAction(string str);
    public static event StringEventAction EventStringt;


    public static void CallStringEvent(string str)
    {
        EventStringt?.Invoke(str);
    }

    #endregion


    #region Events with GameObject Input
    public delegate void GOEventAction(GameObject GO);
    public static event GOEventAction GOEvent;
    public static event GOEventAction ColliderInteractionEvent;
    public static event GOEventAction StartVibrationEvent;

    public static void CallGOEvent(GameObject GO)
    {
        GOEvent?.Invoke(GO);
    }

    public static void CallColliderInteractionEvent(GameObject GO)
    {
        //ColliderInteractionEvent?.Invoke(GO);
        if (ColliderInteractionEvent != null)
        {
            ColliderInteractionEvent.Invoke(GO);
        }
        else
        {
            Debug.LogWarning("No Listener for Call ColliderInteraction");
        }
    }
    public static void CallStartVibrationEvent(GameObject GO)
    {
        if (StartVibrationEvent != null)
        {
            StartVibrationEvent.Invoke(GO);
        }
        else
        {
            Debug.LogWarning("No Listener");
        }
    }

    #endregion


}
