﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

/// <summary>
/// Provides access to configuration data
/// </summary>
public class ConfigurationData
{
	#region Fields

    const string ConfigurationDataFileName = "ConfigurationData.csv";
    Dictionary<ConfigurationDataValueName, float> values =
        new Dictionary<ConfigurationDataValueName, float>();

	#endregion

	#region Properties



    /// <summary>
    ///  Defines if the Laser is used or not
    /// </summary>
    public int UseLaser
    {
        get { return (int)values[ConfigurationDataValueName.UseLaser];}
    }    
    /// <summary>
    /// Defines if the TargetCross is used. If turnd on, Laser is automatically turned on as well!
    /// </summary>
    public int UseTargetCross
    {
        get { return (int)values[ConfigurationDataValueName.UseTargetCross]; }
        set { values[ConfigurationDataValueName.UseTargetCross] = value; }
    }
	
	#endregion

    #region Constructor

    /// <summary>
    /// Constructor
    /// Reads configuration data from a file. If the file
    /// read fails, the object contains default values for
    /// the configuration data
    /// </summary>
    public ConfigurationData()
    {
        // read and save configuration data from file
        StreamReader input = null;
        string currentLine= null;
        try
        {
            // create stream reader object
            input = File.OpenText(Path.Combine(
                Application.streamingAssetsPath, ConfigurationDataFileName));

            // populate values
            currentLine = input.ReadLine();
            while (currentLine != null)
            {
                string[] tokens = currentLine.Split(',');
                ConfigurationDataValueName valueName = 
                    (ConfigurationDataValueName)Enum.Parse(
                        typeof(ConfigurationDataValueName), tokens[0]);
                values.Add(valueName, float.Parse(tokens[1]));
                currentLine = input.ReadLine();
            }
        }
        catch (Exception e)
        {
            // set default values if something went wrong
            Debug.Log(e);
            SetDefaultValues();
            Debug.Log(currentLine);
        }
        finally
        {
            // always close input file
            if (input != null)
            {
                input.Close();
            }
        }
    }

    #endregion

    /// <summary>
    /// Sets the configuration data fields to default values
    /// csv string
    /// </summary>
    void SetDefaultValues()
    {
        Debug.LogWarning("Set Default Values!");
        values.Clear();
        values.Add(ConfigurationDataValueName.UseLaser, 0);
        values.Add(ConfigurationDataValueName.UseTargetCross, 0);
    }
}