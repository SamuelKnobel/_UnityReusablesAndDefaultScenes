﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Collider2D))]
/// <summary>
///  Class to move an Object in different ways.
/// For using, just append it to the GO you want to move, add a second GO-specific Script that references to the Mover-Script to call the mover methods.
/// </summary>
public class Mover : MonoBehaviour
{

    Rigidbody2D rb2D;
    float ColliderHalfeWith;


    Rigidbody2D rb2D;
    public float ColliderHalfeWith;
    public Collider2D ObjectCollider;


    private void Awake()
    {
        ObjectCollider = GetComponent<Collider2D>();

        rb2D = this.GetComponent<Rigidbody2D>();
        if (ObjectCollider.GetType() == typeof(CapsuleCollider2D))
        {
            ColliderHalfeWith = GetComponent<CapsuleCollider2D>().size.y / 2;
        }
        else if (ObjectCollider.GetType() == typeof(CircleCollider2D))
        {
            ColliderHalfeWith = GetComponent<CircleCollider2D>().radius;
        }
        else if (ObjectCollider.GetType() == typeof(BoxCollider2D))
        {
            ColliderHalfeWith = GetComponent<BoxCollider2D>().edgeRadius;
        }
        else
            Debug.LogError("Not Defined Collider: " + this.GetComponent<Collider2D>().ToString());
    }



    /// <summary>
    /// Starts movement in radnom direction
    /// </summary>
    /// <param name="Force"></param>
    /// <param name="forceMode2D"></param>
    public void StarMoveInDirection(float Force, ForceMode2D forceMode2D = ForceMode2D.Force)
    {
        float startAngle = Random.Range(0, Mathf.PI * 2);
        rb2D.AddForce(new Vector2(Mathf.Cos(startAngle), Mathf.Sin(startAngle)) * Force, forceMode2D);
    }

    /// <summary>
    ///  Starts Movement in defined Direction
    /// </summary>
    /// <param name="angle"> in Deg, left is 0°, right is 180°</param>
    /// <param name="Force"></param>
    /// <param name="forceMode2D"></param>
    public void StarMoveInDirection(float angle, float Force, ForceMode2D forceMode2D = ForceMode2D.Force)
    {
        float startAngle = angle * Mathf.Deg2Rad;
        rb2D.AddForce(new Vector2(Mathf.Cos(startAngle), Mathf.Sin(startAngle)) * Force, forceMode2D);
    }

    /// <summary>
    /// Moves Target horizontal
    /// Belonges into the Fixed Update Methode
    /// </summary>
    /// <param name="Axis"> movement axis defined in the Inputmanager</param>
    /// <param name="Speed">Speed of movement</param>
    public void MoveHorizontal(string Axis, float Speed)
    {
        if (Input.GetAxis(Axis) > 0)
        {
            rb2D.MovePosition(transform.position + transform.right * Time.deltaTime * Speed * CalculateClampedX("R"));
        }
        else if (Input.GetAxis(Axis) < 0)
        {
            rb2D.MovePosition(transform.position + -transform.right * Time.deltaTime * Speed * CalculateClampedX("L"));
        }
    }


    public void MoveVertical(string Axis, float Speed)
    {
        if (Input.GetAxis(Axis) > 0)
        {
            rb2D.MovePosition(transform.position + transform.up * Time.deltaTime * Speed * CalculateClampedY("T"));
        }
        else if (Input.GetAxis(Axis) < 0)
        {
            rb2D.MovePosition(transform.position + -transform.up * Time.deltaTime * Speed * CalculateClampedY("B"));
        }
    }

    float CalculateClampedX(string LR)
    {
        float validX = 1;
        switch (LR)
        {
            case "L":
                if (transform.position.x - ColliderHalfeWith < ScreenUtils.ScreenLeft)
                    validX = 0;
                break;
            case "R":
                if (transform.position.x + ColliderHalfeWith > ScreenUtils.ScreenRight)
                    validX = 0;
                break;
            default:
                break;
        }
        return validX;
    }

  float CalculateClampedY(string TB)
    {
        float validX = 1;
        switch (TB)
        {
            case "T":
                if (transform.position.y + ColliderHalfeWith > ScreenUtils.ScreenTop)
                    validX = 0;
                break;
            case "B":
                if (transform.position.y - ColliderHalfeWith < ScreenUtils.ScreenBottom)
                    validX = 0;
                break;
            default:
                break;
        }
        return validX;
    }	
	
}

