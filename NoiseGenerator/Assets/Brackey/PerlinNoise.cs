﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
///  PerlinNoise Generator:
///  Based on  https://www.youtube.com/watch?v=bG0uEXV6aHQ and https://www.youtube.com/watch?v=vFvwyu_ZKfU
/// </summary>
public class PerlinNoise : MonoBehaviour
{

    public int width = 256;
    public int height = 256;


    public float scale = 20f;
    public float offsetX = 100f;
    public float offsetY = 100f;

    private void Start()
    {
        offsetX = Random.Range(0, 9999);
        offsetY = Random.Range(0, 9999);
    }
    private void Update()
    {
        Renderer renderer = GetComponent<Renderer>();
        renderer.material.mainTexture = GenerateTexture();
    }


    Texture2D GenerateTexture()
    {
        Texture2D texture = new Texture2D(width, height);

        // Generate Perlin NoiseMap
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                Color color = CalculateColor(x, y);
                texture.SetPixel(x, y, color);
            }
        }

        texture.Apply();
        return texture;
    }

    Color CalculateColor(int x, int y)
    {
        float xCord = (float)x / width * scale+ offsetX;
        float yCord = (float)y / height * scale + offsetY;
        float sample = Mathf.PerlinNoise(xCord, yCord);
        return new Color(sample, sample, sample);

    }
}
