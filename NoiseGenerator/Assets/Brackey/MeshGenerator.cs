﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Brackey
{

    /// <summary>
    ///  Based on  https://www.youtube.com/watch?v=64NblGkAabk
    /// </summary>
    [RequireComponent(typeof(MeshFilter))]
    public class MeshGenerator : MonoBehaviour
    {

        public int xSize = 50;
        public int zSize = 50;

        public Mesh mesh;
        public Vector3[] vertices;
        public int[] triangles;    // Triangle are Drawn Clockwise
        public float scale;

        // Start is called before the first frame update
        void Start()
        {
            xSize = 50;
            zSize = 50;


            mesh = new Mesh();
            GetComponent<MeshFilter>().mesh = mesh;
            //CreateQuad();
            //StartCoroutine(CreateShape());
            CreateShape();
            UpdateMesh();// 

        }

        // Update is called once per frame
        void Update()
        {
            //UpdateMesh(); // if used with CoRoutine
            CreateShape();
            UpdateMesh();// 

        }


        void CreateQuad()
        {
            vertices = new Vector3[]
            {
            new Vector3(0,0,0),
            new Vector3(0,0,1),
            new Vector3(1,0,0),
            new Vector3(1,0,1)
            };

            triangles = new int[]
            {
            0,1,2,
            1,3,2
            };
        }
        //IEnumerator CreateShape() // To make see the quats be generated
        void CreateShape()
        {
            // VertexCount = (xsize+1) *(zSize+1)
            vertices = new Vector3[(xSize + 1) * (zSize + 1)];


            for (int i = 0, z = 0; z <= zSize; z++)  // 2 variables in one loop, i can only be used in this forloop
            {
                for (int x = 0; x <= xSize; x++)
                {

                    float y = Mathf.PerlinNoise((float)x / xSize, (float)z / zSize) * scale;
                    vertices[i] = new Vector3(x, y, z);
                    i++;

                }
            }
            int vert = 0;
            int tries = 0;
            triangles = new int[xSize * zSize * 6];
            for (int z = 0; z < zSize; z++)
            {
                for (int x = 0; x < xSize; x++)
                {

                    triangles[tries + 0] = vert + 0;
                    triangles[tries + 1] = vert + xSize + 1;
                    triangles[tries + 2] = vert + 1;
                    triangles[tries + 3] = vert + 1;
                    triangles[tries + 4] = vert + xSize + 1; ;
                    triangles[tries + 5] = vert + xSize + 2;
                    vert++;
                    tries += 6;
                    //yield return new WaitForSeconds(.01f);
                }
                vert++;
            }



        }

        void UpdateMesh()
        {
            mesh.Clear();
            mesh.vertices = vertices;
            mesh.triangles = triangles;
            mesh.RecalculateNormals();
        }
        //private void OnDrawGizmos()
        //{
        //    if (vertices == null)
        //        return;
        //    for (int i = 0; i < vertices.Length; i++)
        //    {
        //        Gizmos.DrawSphere(vertices[i], .1f);
        //    }
        //}

    }

}
