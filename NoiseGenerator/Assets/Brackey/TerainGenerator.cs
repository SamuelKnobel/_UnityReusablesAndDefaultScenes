﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerainGenerator : MonoBehaviour
{

    public int depth = 20;  // height

    public int width = 256;
    public int height = 256; // length

    public float scale = 20f;
    public float offsetX = 100f;
    public float offsetY = 100f;

    private void Start()
    {
        offsetX = Random.Range(0, 9999);
        offsetY = Random.Range(0, 9999);
    }

    private void Update()
    {
       Terrain terrain=  GetComponent<Terrain>();
        terrain.terrainData = GenerateTerrain(terrain.terrainData);

        offsetX += Time.deltaTime*2;
    }

    UnityEngine.TerrainData GenerateTerrain(UnityEngine.TerrainData terrainData)
    {

        terrainData.heightmapResolution = width + 1;
        terrainData.size = new Vector3(width, depth, height);
        terrainData.SetHeights(0, 0, GenerateHeight());
        return terrainData;
    }

    float[,] GenerateHeight()
    {
        float[,] heights = new float[width, height];


        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {

                heights[x, y] = CalculateHeight(x,y);
            }
        }
        return heights;

    }


    float CalculateHeight(int x, int y)
    {

        float xCord = (float)x / width * scale + offsetX;
        float yCord = (float)y / height * scale + offsetY;

        return Mathf.PerlinNoise(xCord, yCord);
    }
}
