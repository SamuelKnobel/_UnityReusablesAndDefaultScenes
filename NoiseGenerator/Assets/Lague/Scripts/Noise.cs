﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public static class Noise
//{
//    public enum NormalizeMode
//    {
//        Local, Global
//    };


//    /// <summary>
//    /// Generates a Nosie Map using Perlin Noise
//    /// increasing Lacunarity : more small features
//    /// Persistance: how much do those small features influence the overall shape
//    /// Based on https://www.youtube.com/watch?v=wbpMiKiSKm8
//    /// </summary>
//    /// <param name="mapWidth"></param>
//    /// <param name="mapHeight"></param>
//    /// <param name="seed"></param>
//    /// <param name="scale"></param>
//    /// <param name="octaves"> represents the number of Layers</param>
//    /// <param name="persistance">Controls decrease in amplitude of octaves/Layers (Amplitude = persistance ^ LayerNumber -1), Range[0-1]</param>
//    /// <param name="lacunarity">Controls increase in frequency of octaves (Frequ = Lacunarity ^ LayerNumber-1), Range >1 </param>
//    /// <param name="offset">To scroll trough the Land</param>
//    /// <returns></returns>
//    public static float[,] GenerateNoiseMap(int mapWidth, int mapHeight, int seed, float scale, int octaves, float persistance, float lacunarity, Vector2 offset, NormalizeMode normalizeMode) {

//        float[,] noiseMap = new float[mapWidth, mapHeight];

//        System.Random prng = new System.Random(seed);
//        Vector2[] octaveOffests = new Vector2[octaves];

//        float maxPossibleHeight = 0;
//        float amplitude = 1;
//        float frequency = 1;

//        for (int i = 0; i < octaves; i++)
//        {
//            float offsetX = prng.Next(-100000 , 100000) + offset.x;
//            float offsetY = prng.Next(-100000 , 100000) - offset.y;
//            octaveOffests[i] = new Vector2(offsetX, offsetY);
//            maxPossibleHeight += amplitude;
//            amplitude *= persistance;
//        }
//        if ( scale <= 0) {
//            scale = 0.0001f;
//        }

//        float maxLocalNosieHeight = float.MinValue;
//        float minLocalNosieHeight = float.MaxValue;

//        float halfWidth = mapWidth /2f;
//        float halfHeight = mapHeight/2f;


//        for (int y = 0; y < mapHeight; y++) {
//            for (int x = 0; x < mapWidth; x++)   {

//                amplitude = 1;
//                frequency = 1;
//                float noiseHeight = 0;

//                for (int i = 0; i < octaves; i++)  {
//                    float sampleX = (x- halfWidth + octaveOffests[i].x) / scale* frequency; // more zoomed out -->hight values change more rapidly! Offest makes that the values are taken from different points for each octave
//                    float sampleY = (y- halfHeight + octaveOffests[i].y) / scale* frequency;

//                    float perlinValue = Mathf.PerlinNoise(sampleX, sampleY) *2 -1; // Per Default Perlin Noise Range between 0 and 1 -->to make it more intresting and add negative values --> *2 -1
//                    noiseHeight += perlinValue * amplitude;
//                    amplitude *= persistance;
//                    frequency *= lacunarity;
//                }
//                if (noiseHeight> maxLocalNosieHeight)
//                    maxLocalNosieHeight = noiseHeight;
//                if (noiseHeight < minLocalNosieHeight)
//                    minLocalNosieHeight = noiseHeight;

//                noiseMap[x, y] = noiseHeight;
//            }
//        }

//        // Normalize Noise Map to Values beetween 0 - 1
//        for (int y = 0; y < mapHeight; y++)
//        {
//            for (int x = 0; x < mapWidth; x++)
//            {
//                if (normalizeMode == NormalizeMode.Local)
//                {
//                    noiseMap[x, y] = Mathf.InverseLerp(minLocalNosieHeight, maxLocalNosieHeight, noiseMap[x, y]);
//                }
//                else
//                {
//                    float normalizedHight = (noiseMap[x, y] + 1) / (2f * maxPossibleHeight/2f);
//                    noiseMap[x, y] = Mathf.Clamp(normalizedHight, 0,int.MaxValue);
//                }
//            }
//        }
//                return noiseMap;
//    }


//}

using UnityEngine;
using System.Collections;

public static class Noise
{

	public enum NormalizeMode { Local, Global };

	public static float[,] GenerateNoiseMap(int mapWidth, int mapHeight, int seed, float scale, int octaves, float persistance, float lacunarity, Vector2 offset, NormalizeMode normalizeMode)
	{
		float[,] noiseMap = new float[mapWidth, mapHeight];

		System.Random prng = new System.Random(seed);
		Vector2[] octaveOffsets = new Vector2[octaves];

		float maxPossibleHeight = 0;
		float amplitude = 1;
		float frequency = 1;

		for (int i = 0; i < octaves; i++)
		{
			float offsetX = prng.Next(-100000, 100000) + offset.x;
			float offsetY = prng.Next(-100000, 100000) - offset.y;
			octaveOffsets[i] = new Vector2(offsetX, offsetY);

			maxPossibleHeight += amplitude;
			amplitude *= persistance;
		}

		if (scale <= 0)
		{
			scale = 0.0001f;
		}

		float maxLocalNoiseHeight = float.MinValue;
		float minLocalNoiseHeight = float.MaxValue;

		float halfWidth = mapWidth / 2f;
		float halfHeight = mapHeight / 2f;


		for (int y = 0; y < mapHeight; y++)
		{
			for (int x = 0; x < mapWidth; x++)
			{

				amplitude = 1;
				frequency = 1;
				float noiseHeight = 0;

				for (int i = 0; i < octaves; i++)
				{
					float sampleX = (x - halfWidth + octaveOffsets[i].x) / scale * frequency;
					float sampleY = (y - halfHeight + octaveOffsets[i].y) / scale * frequency;

					float perlinValue = Mathf.PerlinNoise(sampleX, sampleY) * 2 - 1;
					noiseHeight += perlinValue * amplitude;

					amplitude *= persistance;
					frequency *= lacunarity;
				}

				if (noiseHeight > maxLocalNoiseHeight)
				{
					maxLocalNoiseHeight = noiseHeight;
				}
				else if (noiseHeight < minLocalNoiseHeight)
				{
					minLocalNoiseHeight = noiseHeight;
				}
				noiseMap[x, y] = noiseHeight;
			}
		}

		for (int y = 0; y < mapHeight; y++)
		{
			for (int x = 0; x < mapWidth; x++)
			{
				if (normalizeMode == NormalizeMode.Local)
				{
					noiseMap[x, y] = Mathf.InverseLerp(minLocalNoiseHeight, maxLocalNoiseHeight, noiseMap[x, y]);
				}
				else
				{
					float normalizedHeight = (noiseMap[x, y] + 1) / (maxPossibleHeight / 0.9f);
					noiseMap[x, y] = Mathf.Clamp(normalizedHeight, 0, int.MaxValue);
				}
			}
		}

		return noiseMap;
	}

}