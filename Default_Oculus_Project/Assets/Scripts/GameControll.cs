﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class GameControll : MonoBehaviour
{
    Text infoTextField;
    // TODO: Finde Button By Name
    [SerializeField]
    Button startButton;

    TargetSpawner spawner;

    // General Informations
    public static string SubjectID;

    // General Task Skript
    public static Condition currentCondition;
    public static GameState currentState;

    public GameObject currentTarget;


    private void OnEnable()
    {
        EventManager.TriggerEvent += TriggerCalledEvent;
        EventManager.CueEvent += CueCalledEvent;


        //EventManager.ColliderInteractionEvent += StartCountdownTimerTask1;
        //EventManager.ColliderInteractionEvent += StartTimerTask2; ;
    }
    private void OnDisable()
    {
        EventManager.TriggerEvent -= TriggerCalledEvent;
        EventManager.CueEvent -= CueCalledEvent;

        //EventManager.ColliderInteractionEvent -= StartCountdownTimerTask1;
        //EventManager.ColliderInteractionEvent -= StartTimerTask2; ;
    }


    private void Awake()
    {
        currentState = GameState.Initializing;

    }
    void DoNotDestroyOnLoad()
    {
        if (FindObjectOfType<GameControll>() != null)
        {
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            print("multiple scripts found(" + FindObjectsOfType<GameControll>().Length + "), destroy the additional");
            Destroy(this.gameObject);
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        currentState = GameState.MainMenu;
        DoNotDestroyOnLoad();
        OnStart();
        infoTextField = FindObjectOfType<TestControllerInput>().infoText;
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    void OnStart()
    {
        spawner = FindObjectOfType<TargetSpawner>();
        currentCondition = Condition.None;
        //CountdownTimerTask2 = gameObject.AddComponent<Timer>();
        //CountdownTimerTask2.AddTimerFinishedEventListener(spawner.SpawnTarget);
    }


    public void StartTask()
    {
        currentState = GameState.Task;
        spawner.SpawnTarget();
    }


    void TriggerCalledEvent()
    {
        infoTextField.text = infoTextField.text + "\n" + "Trigger Called";
    }

    public void ButtonFunctions(string functionName)
    {
        string currentname = functionName;// EventSystem.current.currentSelectedGameObject.name;


        switch (currentname)
        {
            case "StartButton":
                StartGame();
                break;
            default:
                Debug.LogWarning(currentname + " not defined ");
                break;
        }
    }

    void StartGame()
    {
        startButton.gameObject.SetActive(false);
        infoTextField.text += "\n" + "GameStarted";
        StartTask();
    }

    void CueCalledEvent(float CueType)
    {
        CueType = 2;
        if ( currentTarget !=  null)
        {
            currentTarget.GetComponent<Target>().GiveClue((int)CueType);
            infoTextField.text = infoTextField.text + "\n" + "CueType:" + CueType + " for Object position: " + currentTarget.transform.position;

        }
    }


}
