﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
    [SerializeField]
    public TargetConfiguration targetConfiguration;
    public bool hit;

    Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        //GiveClue();
    }
    public void GiveClue(int CueType)
    {
        switch (CueType)
        {
            case 0:
                // NoCue
                break;
            case 1:
                // AudioCue
                break;
            case 2:
                // VibrationCue
                FindObjectOfType<Tactal_Handler>().HeadBand.Play(this.transform.position);
                break;
            case 3:
                // Combined
                break;
            default:
                break;
        }




        //audioSource = GetComponent<AudioSource>();
        //deathTimer = gameObject.AddComponent<Timer>();
        //deathTimer.AddTimerFinishedEventListener(SelfDestruction);
        //deathTimer.Duration = ConfigurationUtils.TimeBetweenTargets - 0.1f;
        //switch (GameController.currentCondition)
        //{
        //    case Condition.NonSpatialAudio:
        //        audioSource.spatialBlend = 0;
        //        audioSource.Play();
        //        break;
        //    case Condition.SpatialAudio:
        //        audioSource.spatialBlend = 1;
        //        audioSource.Play();
        //        break;
        //    case Condition.Tactile:
        //        EventManager.CallStartVibrationEvent(this.gameObject);
        //        break;
        //    case Condition.Combined:
        //        EventManager.CallStartVibrationEvent(this.gameObject);
        //        audioSource.spatialBlend = 1;
        //        audioSource.Play();
        //        break;
        //    default:
        //        break;
        //}
    }


    void Update()
    {

    }
    private void FixedUpdate()
    {

    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Controller")
        {
            EventManager.CallColliderInteractionEvent(this.gameObject);
        }
    }
}
