﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetSpawner : MonoBehaviour
{

    // ScriptReferences
    GameControll gameControll;

    // Target Prefab
    [SerializeField]
    GameObject Target ;



    void OnEnable()
    {
        EventManager.ColliderInteractionEvent += TargetHit;
        EventManager.TargetShotEvent += TargetShot;
    }
    void OnDisable()
    {
        EventManager.ColliderInteractionEvent -= TargetHit;
        EventManager.TargetShotEvent -= TargetShot;

    }


    // Start is called before the first frame update
    void Start()
    {
        gameControll = FindObjectOfType<GameControll>();

    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.T)|| OVRInput.GetDown(OVRInput.Button.Two)) // B - Button 
        {
            foreach (var t in FindObjectsOfType<Target>())
            {
                Destroy(t.gameObject);
            }
            SpawnTarget();
        }
    }

    void TargetHit(GameObject Object)
    {
        if (Object.tag == "Target")
        {
            if (Object == gameControll.currentTarget)
            {
                gameControll.currentTarget = null;
            }
            Destroy(Object);
            SpawnTarget();
        }
    }

    void TargetShot(GameObject Object)
    {
        if (Object.tag == "Target")
        {
            if (Object == gameControll.currentTarget)
            {
                gameControll.currentTarget = null;
            }
            Destroy(Object);
            SpawnTarget();

        }
    }

   public void SpawnTarget()
    {
        GameObject NewTarget = Instantiate(Target);
        NewTarget.transform.position = new Vector3(Random.Range(-5, 5), 0, 2);

        gameControll.currentTarget = NewTarget;


    }
}
