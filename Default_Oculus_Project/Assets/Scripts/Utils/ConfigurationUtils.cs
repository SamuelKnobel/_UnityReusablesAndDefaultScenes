﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Provides utility access to configuration data
/// </summary>
public static class ConfigurationUtils
{
	#region Fields

	static ConfigurationData configurationData;

	#endregion

	#region Properties

    public static bool UseLaser
    {
        get
        {
            if (configurationData.UseLaser == 1)
                return true;
            else
            {
                configurationData.UseTargetCross = 0;
                return false;
            }
        }
    }
    public static bool UseTargetCross
    {
        get
        {
            if (configurationData.UseTargetCross == 1)
                return true;
            else
                return false;
        }
    }
    #endregion



    #region Public methods

    /// <summary>
    /// Initializes the configuration data by creating the ConfigurationData object 
    /// </summary>
    public static void Initialize()
	{
        configurationData = new ConfigurationData();
	}

	#endregion
}
