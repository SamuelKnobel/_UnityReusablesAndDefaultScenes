DefaultOculusScene
******************


18.06.2020 
---------------------
Update to OculusIntegration Version: 17.0 (27.05.2020)


15.04.2020 
---------------------
Unity Version: 2019.3.3
OculusIntegration Version: 15.0 (06.04.2020)


ChangeLog:
- Update OculusIntegration to Version 15.0
- Update Unity Version to 19.3.9
- Disabled Rotation with thumstick

New Content:
- Add EventSystem and Configuration Utils
- Add TargetSpawner (On Target Shot or on Button B) 
- Add Give Cue Function Skelett (On Button Y)
- Add bHabptics AddOn



10.03.2020
---------------------
Unity Version: 2019.3.3
OculusIntegration Version: 13.0 (7.2.2020)

New Content: 
- basic ExampleScene with full setup of Controller and Camerea.
- Controller have a Laser and TargetCross
- Objects taged with "Target" can be shot
- StartButton to start the game. Starts if shot.
- Ground has a Text element that can be used as Feedback for debugging